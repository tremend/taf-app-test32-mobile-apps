export interface UserModel {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
}

export interface UserResponse {
    id_token: string;
    user_data: UserModel;
}
