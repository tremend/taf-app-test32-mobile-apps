import {EventModel} from './types';
import EventItem from './components/EventItem';
import useEvents from './services';
import {Button} from 'react-native-paper';
import {useEffect, useLayoutEffect, useState} from 'react';
import {FlatList, StyleSheet} from 'react-native';
import React from 'react';
import {View} from 'react-native';

const EventList = ({navigation}: any) => {
    const {getAll} = useEvents();
    const [events, setEvents] = useState<EventModel[]>();



    useEffect(() => {
        getAll().then(setEvents);
    }, []);

    const onItemPress = (item: EventModel) => {
        navigation.navigate('event', {model: item});
    };

    return (
        <View>
            <FlatList
                style={styles.flatListStyle}
                data={events}
                renderItem={({item}) => (
                    <EventItem item={item} onPress={() => onItemPress(item)} />
                )}
                keyExtractor={(item) => `${item.id}`}
            />
        </View>
    );
};

export default EventList;

const styles = StyleSheet.create({
    flatListStyle: {
        paddingHorizontal: 20,
    },
});
