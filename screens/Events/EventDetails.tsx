import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {EventLocationModel, EventModel} from './types';
import EventItem from './components/EventItem';
import useEvents from './services';
import {Card, Paragraph, Surface, Text, Title} from 'react-native-paper';

const EventDetails = ({route}: any) => {
    const {getLocation} = useEvents();
    const [event] = useState<EventModel>(route?.params?.model);
    const [eventLocation, setEventLocation] = useState<EventLocationModel>();

    useEffect(() => {
        if (event?.locationId) {
            getLocation(event?.locationId).then(setEventLocation);
        }
    }, [event]);

    return (
        <View>
            <EventItem item={event} onPress={() => null} />
            <Card style={styles.locationContainer}>
                <Card.Content>
                    <Title>Location</Title>
                    <Paragraph>Name: {eventLocation?.name}</Paragraph>
                    <Paragraph>City: {eventLocation?.city}</Paragraph>
                    <Paragraph>Country: {eventLocation?.country}</Paragraph>
                    <Paragraph>Details: {eventLocation?.details}</Paragraph>
                </Card.Content>
            </Card>
        </View>
    );
};

export default EventDetails;

const styles = StyleSheet.create({
    locationContainer: {
        marginHorizontal: 20,
        padding: 20,
    },
});
