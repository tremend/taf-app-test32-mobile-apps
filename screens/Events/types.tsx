export interface EventModel {
    id: number;
    name: string;
    description: string;
    image: string;
    locationId: number;
}

export interface EventLocationModel {
    id: number;
    name: string;
    details: string;
    city: string;
    country: string;
}
