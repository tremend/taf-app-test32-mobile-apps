import {createStackNavigator} from '@react-navigation/stack';
import * as React from 'react';
import Profile from './Profile';

const Stack = createStackNavigator();

const ProfileFlow = () => {
  return (
    <Stack.Navigator initialRouteName={'profile'}>
      <Stack.Screen
        name="profile"
        options={{title: 'Profile'}}
        component={Profile}
      />
    </Stack.Navigator>
  );
};

export default ProfileFlow;
