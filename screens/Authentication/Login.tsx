import React from 'react';
import {useState} from 'react';
import {StyleSheet, SafeAreaView, View, Image} from 'react-native';
import {TextInput, Button, Surface, Title, useTheme} from 'react-native-paper';
import {useAuth} from '../../contexts/AuthContext';
import {EmailPassword} from './types';

const Login = () => {
    const {colors} = useTheme();

    const {isLoading, signIn} = useAuth();
    const [userData, setUserData] = useState<EmailPassword>({
        email: '',
        password: '',
    });

    const onLogin = async () => {
        try {
            await signIn(userData);
        } catch (e) {
            // Handle error
            console.log(e);
        }
    };

    return (
        <SafeAreaView style={{flex: 1, backgroundColor: colors.background}}>
            <View style={styles.screenContainer}>
                <View style={styles.titleContainer}>
                    <Image
                        style={styles.logo}
                        source={require('../../assets/logo.png')}
                    />
                </View>

                <View style={styles.formContainer}>
                    <Surface style={styles.form}>
                        <Title style={styles.appName}>Test32</Title>
                        <TextInput
                            label="Email"
                            mode="outlined"
                            style={{marginBottom: 10}}
                            onChangeText={(text: string) =>
                                setUserData({...userData, email: text})
                            }
                            keyboardType={'email-address'}
                            autoCapitalize={'none'}
                            autoCorrect={false}
                        />
                        <TextInput
                            label="Password"
                            mode="outlined"
                            onChangeText={(text: string) =>
                                setUserData({...userData, password: text})
                            }
                            secureTextEntry={true}
                        />

                        <Button
                            loading={isLoading}
                            disabled={
                                isLoading ||
                                userData.email === '' ||
                                userData.password === ''
                            }
                            style={styles.button}
                            mode="contained"
                            onPress={onLogin}
                        >
                            Login
                        </Button>
                    </Surface>
                </View>
            </View>
        </SafeAreaView>
    );
};

export default Login;

const styles = StyleSheet.create({
    titleContainer: {
        flex: 0.3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    formContainer: {
        flex: 0.7,
    },
    form: {
        padding: 20,
    },
    button: {
        marginTop: 50,
        height: 50,
        justifyContent: 'center',
    },
    screenContainer: {
        marginHorizontal: 20,
        justifyContent: 'center',
        flex: 1,
    },
    logo: {
        resizeMode: 'contain',
        width: '50%',
        height: '100%',
    },
    appName: {
        marginBottom: 20,
        textAlign: 'center',
    },
});
